﻿<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
if( empty($pageId) ) {
    if( !empty($bo_table) ) {
        $pageId = $bo_table;
    } else if( !empty($co_id) ) {
        $pageId = $co_id;
    } else {
        switch ( $g5['title'] ) {
            case '전체검색 결과' :
                $pageId = 'search_all';
                break;
            case '회원가입약관' :
            case '회원 가입' :
            case '회원가입 완료' :
            case '회원가입 완료' :
            case '회원 정보 수정' :
                $pageId = 'member';
                break;
            default :
                $pageId = 'etc';
                break;
        }
    }
}
function get_page_id( $url, $title = '' ) {
    $pattern = "|bo_table=(.+)|i";
    preg_match($pattern, $url, $match);
    if( !empty($match[1]) ) {
        return $match[1];
    }

    $pattern = "|co_id=(.+)|i";
    preg_match($pattern, $url, $match);
    if( !empty($match[1]) ) {
        return $match[1];
    }

    switch ( $title ) {
        case '전체검색 결과' :
            $pageId = 'search_all';
            break;
        case '회원가입약관' :
        case '회원 가입' :
        case '회원가입 완료' :
        case '회원가입 완료' :
        case '회원 정보 수정' :
            $pageId = 'member';
            break;
        default :
            $pageId = 'etc';
            break;
    }
    return $pageId;
}


?>
<style>
.submenu_depth_1 {display:none;}
.submenu_depth_1.active {display:block;}
.submenu_depth_2.active > .leftmenu_s {color:#8DC700 !important; position: relative;}
.submenu_depth_2.active > .leftmenu_s::after {color:#8DC700 !important; content:''; position: absolute; top: 50%; right: 30px; transform:translateY(-50%); width: 13px; height: 13px; background: url(../img/icon/icon_right.svg) no-repeat center center / contain;}
/* co_id */
#ctt #mysubmenu { position: absolute; left: -290px; top: 30px; width: 240px;}
#mysubmenu::before {content:''; position: absolute; top: -50px; left: 0; width: 88px; height: 28px; background: url(../img/gnb_top.svg) no-repeat center center / contain;}
#mysubmenu ul {list-style:none; font-size:20px; margin:0; padding:0;}
#mysubmenu .leftmenu_b {padding: 12px 20px; background-color:#8DC700; text-align:left; font-size:25px; font-weight:500; color:#fff;}
#mysubmenu .leftmenu_s {position: relative; line-height:35px; padding-left:25px; color: #343A40;}
#mysubmenu a {display: block; text-decoration:none;}
#mysub0 ul > a {padding-top: 25px;}
#mysub0 ul li::before {content:'-'; position: absolute; top: 50%; left: 10px; transform: translateY(-50%);}

/* bo_table */
#bo_list #mysubmenu { position: absolute; left: -290px; top: 30px; width: 240px;}
#bo_gall #mysubmenu { position: absolute; left: -290px; top: 30px; width: 240px;}
</style>

<div id="mysubmenu">
    <?php
    $sql = " select * from {$g5['menu_table']} where me_use = '1' and length(me_code) = '2' order by me_order, me_id ";
    $result = sql_query($sql, false);
    for ($i=0; $row=sql_fetch_array($result); $i++) {
        $chk_page_id = get_page_id($row['me_link'], $row['me_name']);
    ?>
    <ul id="mysub<?php echo $i ?>" class="submenu_depth_1<?php echo $chk_page_id == $pageId ? ' active' : '' ?>">
        <a href="<?php echo $row['me_link']; ?>" target="_<?php echo $row['me_target']; ?>" ><li class="leftmenu_b"><?php echo $row['me_name'] ?></li></a>
            <?php
            $sql2 = " select *
                        from {$g5['menu_table']}
                        where me_use = '1'
                          and length(me_code) = '4'
                          and substring(me_code, 1, 2) = '{$row['me_code']}'
                        order by me_order, me_id ";
            $result2 = sql_query($sql2);
            for ($k=0; $row2=sql_fetch_array($result2); $k++) {
                if($k == 0) echo '<ul>'.PHP_EOL;
                $chk_page_id = get_page_id($row2['me_link'], $row2['me_name']);
            ?>
                <a href="<?php echo $row2['me_link']; ?>" target="_<?php echo $row2['me_target']; ?>" class="submenu_depth_2<?php echo $chk_page_id == $pageId ? ' active' : '' ?>"><li class="leftmenu_s"><?php echo $row2['me_name'] ?></li></a>
            <?php  
            }
            if($k > 0) echo '</ul>'.PHP_EOL;
            ?>
    </ul>
    <?php } ?>
</div>
<script>
$(".submenu_depth_2.active").closest(".submenu_depth_1").addClass("active");
</script>


