<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가


// 개발자 확인용
$arDevMemberIp = array('211.197.113.32','221.140.226.156','175.198.230.29','14.7.102.99','218.155.154.30');

function isdev($me = ''){ // 함수 isdev $me값이 없으면 ''
	global $arDevMemberIp,$REMOTE_ADDR; // 전역변수 $arDevMemberIp,$REMOTE_ADDR

	if($me && $REMOTE_ADDR == '211.197.113.32'){ //if문 $me 그리고 $REMOTE_ADDR이 '211.197.113.32'이면 true 리턴한다.
		return true;
	}//end if
	elseif(in_array($REMOTE_ADDR,$arDevMemberIp)){ // $REMOTE_ADDR 안에 $adDevMemberIp가 있으면 true 리턴
		return true;
	}
	return false; //위조건 만족 안할시 false 리턴
}

// isdev(); //함수실행
// if(isdev()){
// 	error_reporting(E_PARSE);
// 	ini_set('display_errors', '1');
// }

if(isdev() == false){
  goto_url('/hosting_index.html');
}



 ?>
