<?php
if (!defined('_INDEX_')) define('_INDEX_', true);
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

if (G5_IS_MOBILE) {
    include_once(G5_THEME_MOBILE_PATH.'/index.php');
    return;
}

if(G5_COMMUNITY_USE === false) {
    include_once(G5_THEME_SHOP_PATH.'/index.php');
    return;
}

include_once(G5_THEME_PATH.'/head.php');
?>




<?php
?>




<!-------------------------- 슬라이드 -------------------------->
<header>
  <div id="carouselExampleIndicators" class="carousel slide carousel-fade" data-ride="carousel" data-interval="5000">
	<ol class="carousel-indicators">
	  <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
	  <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
	  <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
	</ol>
	<div class="carousel-inner" role="listbox">
	  <!-- Slide One - Set the background image for this slide in the line below -->
	  <div class="carousel-item active" style="background-image: url('https://via.placeholder.com/2560x740')">
		<div class="carousel-caption d-md-block">
		  <h3 class="ks4">에티와이드테마</h3>
		  <p class="ks4 f20">전체페이지를 와이드 형태로만 제작하였습니다.</p>
		</div>
	  </div>
	  <!-- Slide Two - Set the background image for this slide in the line below -->
	  <div class="carousel-item" style="background-image: url('https://via.placeholder.com/2560x740')">
		<div class="carousel-caption d-md-block">
		  <h3 class="ks4">반응형 비즈니스 테마</h3>
		  <p class="ks4 f20">CMS 인 그누보드 5.4 와 연동되어 사용가능한 테마 입니다.</p>
		</div>
	  </div>
	  <!-- Slide Three - Set the background image for this slide in the line below -->
	  <div class="carousel-item" style="background-image: url('https://via.placeholder.com/2560x740')">
		<div class="carousel-caption d-md-block">
		  <h3 class="ks4">테마몰 오픈</h3>
		  <p class="ks4 f20">테마몰을 오픈하였습니다.</p>
		</div>
	  </div>
	</div>
	<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
	  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
	  <span class="sr-only">Previous</span>
	</a>
	<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
	  <span class="carousel-control-next-icon" aria-hidden="true"></span>
	  <span class="sr-only">Next</span>
	</a>
  </div>
</header>
<!-------------------------- ./슬라이드 -------------------------->

<div class="inner">
    <!-------------------------- 아이콘박스 -------------------------->
    <div class="container">
        <div class="row padding-top-100">
            <a href="<?php echo G5_URL; ?>/bbs/content.php?co_id=company" class="col-lg-3 col-md-3 col-sm-12 col-12">
                <div class="box">
                    <div class="icon">
                        <div class="info">
                            <i><img src="<?php echo G5_IMG_URL; ?>/icon/bg-icon1.png" alt=""></i>
                            <h2 class="ko_20">무슨 일을 하시나요?</h2>
                            <p class="ko_18">인천장애인성폭력상담소의 주요활동을 소개합니다.</p>
                        </div>
                    </div>
                </div>
            </a><!-- ./col -->
            <a href="<?php echo G5_URL; ?>/bbs/board.php?bo_table=media" class="col-lg-3 col-md-3 col-sm-12 col-12">
                <div class="box">
                    <div class="icon">
                        <div class="info">
                            <i><img src="<?php echo G5_IMG_URL; ?>/icon/bg-icon2.png" alt=""></i>
                            <h2 class="ko_20">언론보도</h2>
                            <p class="ko_18">인천장애인성폭력상담소의 언론보도를 확인하세요.</p>
                        </div>
                    </div>
                </div>
            </a><!-- ./col -->
            <a href="<?php echo G5_URL; ?>/bbs/content.php?co_id=support" class="col-lg-3 col-md-3 col-sm-12 col-12">
                <div class="box">
                    <div class="icon">
                        <div class="info">
                            <i class="icon"><img src="<?php echo G5_IMG_URL; ?>/icon/bg-icon3.png" alt=""></i>
                            <h2 class="ko_20">지원내용</h2>
                            <p class="ko_18">인천장애인성폭력상담소의 지원내용을 소개합니다.</p>
                        </div>
                    </div>
                </div>
            </a><!-- ./col -->
            <a href="<?php echo G5_URL; ?>/bbs/content.php?co_id=sponsorship" class="col-lg-3 col-md-3 col-sm-12 col-12">
                <div class="box">
                    <div class="icon">
                        <div class="info">
                            <i><img src="<?php echo G5_IMG_URL; ?>/icon/bg-icon4.png" alt=""></i>
                            <h2 class="ko_20">후원안내</h2>
                            <p class="ko_18">후원금은 피해자지원과 활동비로 쓰입니다.</p>
                        </div>
                    </div>
                </div>
            </a><!-- ./col -->
        </div><!-- /row -->

        <div class="d-none d-sm-block margin-top-30"></div><!-- pc 만 적용 -->
    </div><!-- /container -->

    <!-------------------------- 게시판 -------------------------->
    <div class="container main-board-container">
        <div class="row">
            <div class="col-lg-6">
                <div class="consult-cont">
                    <h2 class="ko">상담전화</h2>
                    <i class="ko">032-424-1366</i>
                    <p class="ko">평일 am10:00 - pm17:00</p>
                    <p class="ko">주말 및 공휴일 휴관</p>
                    <a href="<?php echo G5_URL; ?>/bbs/board.php?bo_table=cyber">상담 바로가기 <i></i></a>
                </div>
            </div>
            <div class="row col-lg-6">
                <div id="TAB_A" class="rumitab_boxs">
                    <ul class="tabs">
                        <li class="A_tab1 on">공지사항</li>
                        <li class="A_tab2">자유게시판</li>
                    </ul>
                    <div class="tab_container">
                        <div class="A_tab1_content"><?php echo latest('theme/basic_main_one', 'notice', 5, 40);?></div>
                        <div class="A_tab2_content"><?php echo latest('theme/basic_main_one', 'free', 5, 40);?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-------------------------- GALLERY -------------------------->
    <!--

    테마폴더/tail.php : 43 번째줄에서 수정하시면 됩니다.
    owlcarousel 시간조정, 개수조정, 오토플레이 조정
    -->

    <div class="container margin-top-100 padding-bottom-120">
        <div class="swiper footer-icon">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <a href="#none">
                        <img src="../img/bg/swiper/s-1.png" alt="">
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="#none">
                        <img src="../img/bg/swiper/s-2.png" alt="">
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="#none">
                        <img src="../img/bg/swiper/s-3.png" alt="">
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="#none">
                        <img src="../img/bg/swiper/s-1.png" alt="">
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="#none">
                        <img src="../img/bg/swiper/s-2.png" alt="">
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="#none">
                        <img src="../img/bg/swiper/s-3.png" alt="">
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="#none">
                        <img src="../img/bg/swiper/s-1.png" alt="">
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="#none">
                        <img src="../img/bg/swiper/s-2.png" alt="">
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="#none">
                        <img src="../img/bg/swiper/s-3.png" alt="">
                    </a>
                </div>
            </div>
            <div class="swiper-pagination"></div>
        </div>
    </div>
</div>
<!-- end: .inner -->
<script>
    //main_icon swiper
    var swiper = new Swiper(".footer-icon", {
        slidesPerView: 'auto',
        spaceBetween: 30,
        loop: true,
        parallax: true,
        speed: 100,
        autoplay: {
            delay: 1000,
            disableOnInteraction: false,
          },
        pagination: {
          el: ".swiper-pagination",
          clickable: true,
        },
      });
</script>

<?php
include_once(G5_THEME_PATH.'/tail.php');
