	<footer class="py-5">
		<div class="container footer">
			<div class="row">
                <div class="col-lg-6">
                    <p class="navbar-brand">
                        <img src="http://www.icsung1366.or.kr/theme/theme_wide_16/img/logo.jpg" title="">
                    </p>
                </div>
                <div class="col-lg-6">
                    <p class="ks2 f12">
						<i class="far fa-building"></i> 사무실 : 경기도 아름다운 아파트 101동 1004호<br />
						<i class="fas fa-phone"></i> 연락처 : 010-0000-0000<br />
						<i class="far fa-envelope-open"></i> <a href="mailto:softzonecokr@naver.com" class="color-white">Email : SOFTZONECOKR@NAVER.COM</a><br />
						<i class="fas fa-fax"></i> 팩스번호 : 02) 1234-1234<br />
					</p>
                </div>
			</div>
		</div><!--/container-->
    </footer>